# yo2kam qrz.com page

An attempt to restructure & redesign the YO2KAM radio club iframe content on qrz.com page.

## Objectives

- Replace table layout with css grid layout
- Add better font
- Choose better colors
- Stylize anchors
- Shadow to images ?
- Partners on top / right column
- Move other things on the right column ?
- And yes .... responsiveness

## Online test

https://jsfiddle.net/zmoky/5havu1w3/33/

## Target

https://www.qrz.com/db/YO2KAM
